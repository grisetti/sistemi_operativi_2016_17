#pragma once

int* IntArray_alloc(int size);

void IntArray_free(int* v);

void IntArray_set(int* v, int size, int value);

void IntArray_sum(int* dest, int* src, int size);

void IntArray_sub(int* dest, int* src, int size);

void IntArray_copy(int* dest, int* src, int size);

int IntArray_isLess(int* dest, int* src, int size);

void IntArray_print(int* v, int size);
