#include <stdio.h>
#include <stdlib.h> 
#include <assert.h>
#include "bankers_allocator.h"

void BankersAllocator_init(BankersAllocator* a,
			   int num_processes,
			   int num_resources,
			   int* resources_available){
  a->num_processes=num_processes;
  a->num_resources=num_resources;
  IntMatrix_alloc(&a->resources_max, num_processes, num_resources);
  IntMatrix_alloc(&a->resources_allocated, num_processes, num_resources);
  a->resources_available=IntArray_alloc(num_resources);
  IntArray_copy(a->resources_available, resources_available, num_resources);
}

int BankersAllocator_isSafe(int* safe_order, BankersAllocator* a) {
  int np=a->num_processes;
  int nr=a->num_resources;
  
  int work[nr];
  // copy resources
  IntArray_copy(work, a->resources_available, nr);

  for (int p=0; p<np; ++p)
    safe_order[p]=p;
  // look for an unfinished process we can satisfy
  
  int op=0; // this is the index of the first free cell of order
  int next_op=1; // this is the index of the first "available" process (greatet than op)
  while (op<np) {
    
    int p=safe_order[op]; // we pick the first unassigned process
    int needed[nr];
    IntArray_copy(needed, a->resources_max.row_ptrs[p], nr);
    IntArray_sub(needed, a->resources_allocated.row_ptrs[p], nr);
    printf("\n*******************************************\n");
    
    printf("op: %d\n", op);
    printf("next_op: %d\n", next_op);
    printf("p: %d\n", p);
    printf("work: \n");
    IntArray_print(work, nr);
    printf("needed: \n");
    IntArray_print(needed, nr);

    // if we allocate a process, we put it in the sequence
    if (IntArray_isLess(needed, work, nr)){
      IntArray_sum(work, a->resources_allocated.row_ptrs[p], nr); // we simulate the release of the resource of this process
      safe_order[op]=p;
      ++op;
      next_op=op+1;
      printf("accept\n");
    } else {
      // we swap the current process with one in the remaining part of the list, if possible
      if (next_op<np) {
	int aux=safe_order[op];
	safe_order[op]=safe_order[next_op];
	safe_order[next_op]=aux;
	next_op++;
	printf("reject\n");
      } else {
	printf("unsafe\n");
	return 0;
      }
    }
  }
  return op==np;
}

int BankersAllocator_tryAlloc(int* safe_order,
			      BankersAllocator* a,
			      int p, int* requested_resources){
  int nr=a->num_resources;

  // process not in the list
  if (p<a->num_processes) {
    assert(0 && "PROCESS OUT OF BOUNDS");
    return -1;
  }

  // we check if the process asked more than allowed
  int newly_allocated_resources[nr];
  IntArray_copy(newly_allocated_resources,
		requested_resources,
		nr);

  IntArray_sum(newly_allocated_resources,
	       a->resources_allocated.row_ptrs[p],
	       nr);

  if (! IntArray_isLess(newly_allocated_resources,
			a->resources_max.row_ptrs[p], nr) ) {
    assert(0 && "ILLEGAL REQUEST");
    return 0; // not possible to satisfy 
  }
  
  // we backup the allocation;
  int old_resources[nr];
  IntArray_copy(old_resources, a->resources_allocated.row_ptrs[p], nr);  
  //we simulate giving the process the resources
  IntArray_sum(a->resources_allocated.row_ptrs[p], requested_resources, nr);
  int alloc_result=BankersAllocator_isSafe(safe_order, a);

  if (alloc_result) {
    // allocation went well, we give the process the resources
    IntArray_sub(a->resources_available, requested_resources, nr);
    IntArray_sum(a->resources_allocated.row_ptrs[p], requested_resources, nr);
    return 1;
  }

  // things went bad, we restore backup
  IntArray_copy(a->resources_allocated.row_ptrs[p], old_resources, nr);  
  return 0;
}

