#pragma once

typedef struct IntMatrix {
  int rows;
  int cols;
  int* buffer;
  int ** row_ptrs;
} IntMatrix;


void IntMatrix_alloc(IntMatrix* m, int rows, int cols);

void IntMatrix_free(IntMatrix* m);

void IntMatrix_copy(IntMatrix* dest, IntMatrix* src);

void IntMatrix_print(IntMatrix* m);
