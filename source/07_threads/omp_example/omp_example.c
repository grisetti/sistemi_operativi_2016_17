#include <omp.h>
#include <stdio.h>

int main(int argc, char** argv) {
  const int size=1024;
  int v[size];
  int num_threads=omp_get_max_threads();
  printf("spawning %d threads\n", num_threads);
#pragma omp parallel
  { int thread_num=omp_get_thread_num();
    printf("thread_num: %d\n", thread_num);
    for(int i=thread_num; i<size; i+=num_threads)
      v[i]=1;
  }
  
#pragma omp parallel
  {
    printf("I am in a parallel section\n");
  }

  int v_sum[num_threads];
#pragma omp parallel
  { int thread_num=omp_get_thread_num();
    v_sum[thread_num]=0;
    for(int i=thread_num; i<size; i+=num_threads)
      v_sum[thread_num]+=v[i];
  }

  for (int i=1; i<num_threads;  ++i){
    v_sum[0]+=v_sum[i];
  }

  printf("v_sum: %d\n", v_sum[0]);
}
