#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

typedef struct ThreadOneParameters{
  char* msg;
  int iterations;
  int cancellation_type;
} ThreadOneParameters;

typedef struct ThreadTwoParameters{
  int datum;
  int iterations;
  int cancellation_type;
} ThreadTwoParameters;


void* threadFunctionOne(void* args) {
  ThreadOneParameters* params=(ThreadOneParameters*)args;
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  pthread_setcanceltype(params->cancellation_type, NULL);
  
  for (int i=0; i<params->iterations; ++i){
    printf("T1: iteration=%d, msg:%s\n", i, params->msg); // <cancellation point :)
    sleep(1); // < this is also a cancellation point
  }
  return 0;
}

void* threadFunctionTwo(void* args) {
  ThreadTwoParameters* params=(ThreadTwoParameters*)args;
  pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
  pthread_setcanceltype(params->cancellation_type, NULL);
  for (int i=0; i<params->iterations; ++i){
    printf("T2: iteration=%d, data:%d\n", i, params->datum); //< this is a cancellation point
    for (size_t k=0; k<100000000000; ++k);                   //< this is not a cancellation point
                                                             //< when execution is here 
                                                             //< deferred cancellation does not work
  }
  return 0;
}

int main(int argc, char** argv) {
  // needed variables
  pthread_t t1;
  pthread_attr_t attr1;
  pthread_attr_init(&attr1);
  ThreadOneParameters param1;
  param1.iterations=10;
  param1.msg="sistemi operativi";
  param1.cancellation_type=PTHREAD_CANCEL_DEFERRED;
  
  pthread_t t2;
  pthread_attr_t attr2;
  pthread_attr_init(&attr2);
  ThreadTwoParameters param2;
  param2.iterations=10;
  param2.datum=41;
  //param2.cancellation_type=PTHREAD_CANCEL_ASYNCHRONOUS;

  // what happens if we uncomment the following?
  param2.cancellation_type=PTHREAD_CANCEL_DEFERRED;
  
  
  int t1_start_result = pthread_create(&t1, &attr1,
				       threadFunctionOne, &param1);

  int t2_start_result = pthread_create(&t2, &attr2,
				       threadFunctionTwo, &param2);

  int rounds = 5;
  for(int i=0; i<rounds; ++i){
    printf("P: waiting %d\n",i);
    sleep(1);
  }

  printf("sending cancellation to threads");
  pthread_cancel(t1);
  pthread_cancel(t2);
  
  void* retval1;
  pthread_join(t1, &retval1);
  printf("joined with T1");
  
  void* retval2;
  pthread_join(t2, &retval2);
  printf("joined with T2");

  pthread_attr_destroy(&attr1);
  pthread_attr_destroy(&attr2);

  exit(0);

}
