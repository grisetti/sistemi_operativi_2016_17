#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
#include <unistd.h>

typedef struct 
{
  int id;
  unsigned long sleep_ms;
} ThreadParams;


void *workAndSleep(void * par) {
  ThreadParams *th_param = (ThreadParams*)par;
  
  usleep(500000);
  while (1) {     
    printf("THREAD %d : Running \n", th_param->id);
    
    /* Il thread entra in un lungo CPU-burst, saturando la CPU */
    int i=0;
    for(i=0; i<1000000000; i++) {
      /* Ogni tanto notifica a video dov'è arrivato (chiamando una syscall) */
      if (!(i%200000000))
	printf("THREAD %d : i = %d\n",th_param->id,i);
    }
    printf("THREAD %d : Sleep\n\n",th_param->id);
    
    /* Si ferma per qualche istante (chiamando una syscall) */
    usleep( th_param->sleep_ms * 1000 );
  }
  pthread_exit(NULL);
}


int main(int argc,char **argv) { 


  pthread_t thread1, thread2, thread3, thread4;
  ThreadParams tp1 = {1,3000}, tp2 = {2,3000}, 
  tp3 = {3,1000}, tp4 = {4,1000};
	       
  pthread_attr_t attr;
  struct sched_param param;
  memset(&param,0,sizeof(param));
  
  pthread_attr_init(&attr);
  
  
  /* Impongo che thread siano eseguiti su di una sola CPU */
  cpu_set_t cpuset;
  CPU_ZERO(&cpuset);
  CPU_SET(0, &cpuset);
  pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpuset);
  
  
  /* Lancio i thread 1 e 2 */
  if (pthread_create(&thread1, &attr, workAndSleep, &tp1)) {
    printf("error in creating thread 1\n");
  }
  if (pthread_create(&thread2, &attr, workAndSleep, &tp2)) {
    printf("error in creating thread 2\n");
  }
  
  
  /* Lancio i thread 3 e 4 */
  if (pthread_create(&thread3, &attr, workAndSleep, &tp3)){
    printf("error in creating thread 3\n");
  }
  if (pthread_create(&thread4, &attr, workAndSleep, &tp4)){
    printf("error in creating thread 4\n");
  }

  /* Attendo la terminazione di tutti i thread 
   * (in realtà i thread non terminano)*/
  pthread_join(thread1,NULL);
  pthread_join(thread2,NULL);
  pthread_join(thread3,NULL);
  pthread_join(thread4,NULL);

  pthread_exit(NULL);
}
