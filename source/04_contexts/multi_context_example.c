#include <ucontext.h>
#include <stdio.h>
#include <stdlib.h>

#define STACK_SIZE 16384
const int num_iterations=5;
ucontext_t main_context;
const int max_contexts=100;

typedef void (*ContextFnType)(void*);
typedef void (*UContextFnType)();

typedef struct ContextWithStack {
  ucontext_t context;
  char stack[STACK_SIZE] __attribute__ ((aligned (16)));
  ContextFnType fn;
  int number;
  ucontext_t* next_context;
} ContextWithStack;

ContextWithStack* ContextWithStack_alloc(int number, ContextFnType fn) {
  ContextWithStack* cws = (ContextWithStack*) malloc(sizeof(ContextWithStack));
  cws->fn=fn;
  cws->number=number;

  getcontext(&cws->context);
  cws->context.uc_stack.ss_sp=cws->stack;
  cws->context.uc_stack.ss_size = STACK_SIZE;
  cws->context.uc_stack.ss_flags = 0;
  cws->context.uc_link=&main_context;
  cws->next_context=0;
  makecontext(&cws->context, (UContextFnType) cws->fn, 1, cws);
  return cws;
}

void contextFn(void* arg) {
  ContextWithStack* cws=(ContextWithStack*)arg;
  printf("Context [%d]: started\n", cws->number);
  for (int i=0; i<num_iterations; ++i){
    printf("Context [%d]: [%d]\n",cws->number, i);
    if (cws->next_context)
      swapcontext(&cws->context, cws->next_context);
  }
  swapcontext(&cws->context, &main_context);
}
int main(int argc, char** argv){
  if (argc<2){
    printf("usage: %s <int n>: spawns n contexts and jumps between them. n in [1..100] \n", argv[0]);
    exit(0);
  }
  int num_contexts=atoi(argv[1]);
  if (num_contexts<0) {
    printf(" n must be positive\n");
    exit(0);
  }
  if (num_contexts>max_contexts) {
    printf(" n must be <100 \n");
    exit(0);
  }
  //create a circular list of contexts
  ContextWithStack* first=0;
  ContextWithStack* last=0;
  for (int c=0; c<num_contexts; ++c) {
    ContextWithStack* new_cws=ContextWithStack_alloc(c, contextFn);
    if (! first) {
      first=last=new_cws;
    }
    last->next_context=&new_cws->context;
    last= new_cws;
  }
  last->next_context=&first->context;
  printf("run baby run\n");
  fflush(stdout);
  swapcontext(&main_context, &first->context); // we will jump back here
  printf("exiting\n");
}
