#pragma once

typedef struct Mutex {
  int lock;
} Mutex;

typedef struct Semaphore{
  Mutex mutex;
  int count;
} Semaphore;


// initializes a mutex object
// always call it before using it
void Mutex_init(Mutex* m);

// locks a mutex
// process is put in busy waiting
void Mutex_lock(Mutex* m);

// unlocks a mutex
void Mutex_unlock(Mutex* m);

// tries to unlock a mutex, returns 0 if success
int Mutex_trylock(Mutex* m);

// initalizes a semaphore obect
void Semaphore_init(Semaphore* s, int value);

// busy waits (yeah...)
// on a semaphore
void Semaphore_wait(Semaphore* s);

//tries to acquire a lock
//returns 0 on success
int Semaphore_trywait(Semaphore* s);

// increments semaphore by 1
void Semaphore_post();
