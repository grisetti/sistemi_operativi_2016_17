#pragma once
#include <pthread.h>
#include "sync_primitives.h"

typedef struct FixedSizeMessageQueue{
  char** messages;
  int size;
  int size_max;
  int front_idx;
  Semaphore sem_full;
  Semaphore sem_empty;
  Mutex mutex;
} FixedSizeMessageQueue;

void FixedSizeMessageQueue_init(FixedSizeMessageQueue* q,
				int size_max);

void FixedSizeMessageQueue_destroy(FixedSizeMessageQueue* q);

void FixedSizeMessageQueue_pushBack(FixedSizeMessageQueue*q,
				    char* message);

char* FixedSizeMessageQueue_popFront(FixedSizeMessageQueue*q);

int FixedSizeMessageQueue_sizeMax(FixedSizeMessageQueue* q);

int FixedSizeMessageQueue_size(FixedSizeMessageQueue* q);
